

## readme

### 编译器
i386-elf-gcc
来源：https://github.com/nativeos/i386-elf-toolchain/releases

### 镜像写入工具
mkimg144.c
来源：https://github.com/alexfru/BootProg

### make 工具
来自30天自制操作系统里的，因为我本机的make打包貌似有问题

### 开发环境
- MSYS
- bochs

### 注意
1. 修改orange文件夹makefile中 bochs 虚拟机目录的路径(比如我安装到c盘就是 “/C/Program Files (x86)/Bochs-2.6.10/”)

2. 在目录下建立一个i386-elf-gcc（随你命名）先将 i386-elf-gcc-windows-x86_64.zip 解压的东西放进去
再将i386-elf-binutils-windows-x86_64.zip 解压的东西和i386-elf-gcc合并。
（简而言之 bin放在bin， elf放在elf）虽然官网说是放到你的path上，但实际上只要调用就好,详细看makefile。

### 使用

在oranges 下打开cmd
- make mkTool (这是用来写镜像的工具 详细看oranges/tools的mkimg144.c代码)
- make run